## Commits & Merges

Auto-generated semver, this means commit messages need to have a specific format as defined at [Conventional Commit Guide](https://www.conventionalcommits.org/en/v1.0.0-beta.3/). Specific examples for the tool [Semrel-Gitlab](https://juhani.gitlab.io/go-semrel-gitlab/commit-message/)

```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

### Supported Types in Commit Messages
See below for details regarding the supported types in commit messages:
   * `chore` - prefixed commits change nothing
   * `fix,refactor,perf,docs,style,test,bug` prefixed commits bump the patch version
   * `feat,feature,story` prefixed commits bump the minor version
   * adding `BREAKING CHANGE: short summary` to any commit will bump the major version (after v1.0.0 has be tagged, other wise it bumps minor)

After the master pipeline finishes successfully it will tag that commit with the correct semver based on the commit messages since the last tag. 