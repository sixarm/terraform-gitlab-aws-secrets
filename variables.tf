variable "aws_user_id" {
    description = "The AWS_ACCESS_KEY_ID"
}
variable "aws_user_secret" {
    description = "The AWS_SECRET_ACCESS_KEY"
}
variable "gitlab_id" {
    description = "The ID of the Gitlab Project or Group"
}
variable "gitlab_group" {
    description = "Boolean: Is this a Gitlab Group? Default false."
    default = false
}
variable "aws_account_alias" {
    description = "Alias name for AWS account matching terraform workspace name"
    default = "default"
}
variable "aws_user_name" {
    description = "AWS user assocated with the access key"
    default = ""
}
variable "gitlab_var_prefix" {
    description = "Prefix for the Gitlab variable (i.e.: <Prefix>AWS_ACCESS_KEY_ID)"
    default = ""
}
