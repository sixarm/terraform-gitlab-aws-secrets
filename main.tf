
/*

*/

// if gitlab_group is false 
resource "gitlab_project_variable" "id" {
  count = "${var.aws_account_alias == terraform.workspace && ! var.gitlab_group ? 1 : 0}"
  project = "${var.gitlab_id}"
  key = "${var.gitlab_var_prefix == "" ? "" : var.gitlab_var_prefix }AWS_ACCESS_KEY_ID"
  value = "${var.aws_user_id}"
}
resource "gitlab_project_variable" "secret" {
  count = "${var.aws_account_alias == terraform.workspace && ! var.gitlab_group ? 1 : 0}"
  project = "${var.gitlab_id}"
  key = "${var.gitlab_var_prefix == "" ? "" : var.gitlab_var_prefix  }AWS_SECRET_ACCESS_KEY"
  value = "${var.aws_user_secret}"
}
resource "gitlab_project_variable" "user" {
  count = "${var.aws_account_alias == terraform.workspace && var.aws_user_name != "" && ! var.gitlab_group ? 1 : 0}"
  project = "${var.gitlab_id}"
  key = "${var.gitlab_var_prefix == "" ? "" : var.gitlab_var_prefix  }AWS_USER_NAME"
  value = "${var.aws_user_name}"
}
resource "gitlab_project_variable" "account" {
  count = "${var.aws_account_alias == terraform.workspace && ! var.gitlab_group ? 1 : 0}"
  project = "${var.gitlab_id}"
  key = "${var.gitlab_var_prefix == "" ? "" : var.gitlab_var_prefix  }AWS_ACCOUNT_ALIAS"
  value = "${var.aws_account_alias}"
}
// if gitlab_group is true
resource "gitlab_group_variable" "id" {
  count = "${var.aws_account_alias == terraform.workspace && var.gitlab_group ? 1 : 0}"
  group = "${var.gitlab_id}"
  key = "${var.gitlab_var_prefix == "" ? "" : var.gitlab_var_prefix }AWS_ACCESS_KEY_ID"
  value = "${var.aws_user_id}"
}
resource "gitlab_group_variable" "secret" {
  count = "${var.aws_account_alias == terraform.workspace && var.gitlab_group ? 1 : 0}"
  group = "${var.gitlab_id}"
  key = "${var.gitlab_var_prefix == "" ? "" : var.gitlab_var_prefix  }AWS_SECRET_ACCESS_KEY"
  value = "${var.aws_user_secret}"
}
resource "gitlab_group_variable" "user" {
  count = "${var.aws_account_alias == terraform.workspace && var.aws_user_name != "" && var.gitlab_group ? 1 : 0}"
  group = "${var.gitlab_id}"
  key = "${var.gitlab_var_prefix == "" ? "" : var.gitlab_var_prefix  }AWS_USER_NAME"
  value = "${var.aws_user_name}"
}
resource "gitlab_group_variable" "account" {
  count = "${var.aws_account_alias == terraform.workspace && var.gitlab_group ? 1 : 0}"
  group = "${var.gitlab_id}"
  key = "${var.gitlab_var_prefix == "" ? "" : var.gitlab_var_prefix  }AWS_ACCOUNT_ALIAS"
  value = "${var.aws_account_alias}"
}